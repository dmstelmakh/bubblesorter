package ru.inordic.arj.mvn.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BubbleSorterTest {
    @Test
    void testSort() {
        int[] src = new int[]{5, 4, 2, 8, 10};
        int[] expectedResult = new int[]{2, 4, 5, 8, 10};
        Sorter bubbleSorter = new BubbleSorter();
        int[] result = bubbleSorter.sort(src);
        Assertions.assertArrayEquals(expectedResult, result);
    }

}
