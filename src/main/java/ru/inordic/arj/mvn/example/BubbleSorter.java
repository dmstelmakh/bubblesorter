package ru.inordic.arj.mvn.example;

public class BubbleSorter implements Sorter {
    @Override
    public int[] sort(int[] src) {
        int[] copy = new int[src.length];
        for (int i = 0; i < src.length; i++) {
            copy[i] = src[i];

        }

//        int[] copy =  Arrays.copyOf(src, src.length);
        boolean c;
       do {
           c = false;
           for (int i = 0; i < copy.length - 1; i++) {
               if (copy[i] > copy[i + 1]) {
                   int r = copy[i + 1];
                   copy[i + 1] = copy[i];
                   copy[i] = r;
                   c = true;
               }
           }
       } while (c);

        return copy;
    }
}
