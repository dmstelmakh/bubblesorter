package ru.inordic.arj.mvn.example;

public abstract class AbstractSorter implements Sorter {

    @Override
    public final int[] sort(int[] src) {
        int[] copy = new int[src.length];
        for (int i = 0; i < src.length; i++) {
            copy[i] = src[i];
        }
        return internalSort(copy);
    }

    protected abstract int[] internalSort(int[] copy);
}
