package ru.inordic.arj.mvn.example;

public class MixMaxResult {

    private String min;
    private String max;

    public MixMaxResult(String min, String max) {
        this.min = min;
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public String getMax() {
        return max;
    }



}
