package ru.inordic.arj.mvn.example;

public interface Sorter {
    int [] sort(int [] src);

}
