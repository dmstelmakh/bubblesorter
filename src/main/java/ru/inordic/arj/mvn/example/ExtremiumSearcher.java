package ru.inordic.arj.mvn.example;

import java.util.Calendar;

public class ExtremiumSearcher {


    //     abc tred age
    public MixMaxResult search(String str) {

        char[] arr = str.toCharArray();
        StringBuilder buf = new StringBuilder();
        String candidateForMin = null;
        String candidateForMax = null;
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if (Character.isWhitespace(c) && buf.length() > 0) {
                String word = buf.toString();
                buf.setLength(0);
                int cnt = word.length();
                if (candidateForMin != null) {
                    candidateForMin = word;
                } else if (cnt > candidateForMax.length()) {
                    candidateForMax = word;
                }

            } else {
                buf.append(c);
            }
        }
        return null;
    }
}
